<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Salariati\models\Salariati */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Salariati',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Salariatis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="salariati-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
