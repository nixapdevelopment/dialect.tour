<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Salariati\models\SalariatiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="salariati-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Nume') ?>

    <?= $form->field($model, 'Prenume') ?>

    <?= $form->field($model, 'PunctDeLucru') ?>

    <?= $form->field($model, 'Functie') ?>

    <?php // echo $form->field($model, 'DataAngajarii') ?>

    <?php // echo $form->field($model, 'Tip') ?>

    <?php // echo $form->field($model, 'FunctieBaza') ?>

    <?php // echo $form->field($model, 'NormaPeZi') ?>

    <?php // echo $form->field($model, 'OrePeLuna') ?>

    <?php // echo $form->field($model, 'CASIndividuala') ?>

    <?php // echo $form->field($model, 'CASAngajator') ?>

    <?php // echo $form->field($model, 'ContrSomajIndividuala') ?>

    <?php // echo $form->field($model, 'ContrSomajAngajator') ?>

    <?php // echo $form->field($model, 'ContrFGCS') ?>

    <?php // echo $form->field($model, 'ContrAccMunca') ?>

    <?php // echo $form->field($model, 'ZileCOAn') ?>

    <?php // echo $form->field($model, 'TipSalariu') ?>

    <?php // echo $form->field($model, 'Avans') ?>

    <?php // echo $form->field($model, 'SalariuBrut') ?>

    <?php // echo $form->field($model, 'SalariuOrar') ?>

    <?php // echo $form->field($model, 'CNP') ?>

    <?php // echo $form->field($model, 'Judet') ?>

    <?php // echo $form->field($model, 'Localitate') ?>

    <?php // echo $form->field($model, 'Strada') ?>

    <?php // echo $form->field($model, 'Numar') ?>

    <?php // echo $form->field($model, 'CodPostal') ?>

    <?php // echo $form->field($model, 'Bloc') ?>

    <?php // echo $form->field($model, 'Scara') ?>

    <?php // echo $form->field($model, 'Etaj') ?>

    <?php // echo $form->field($model, 'Apartament') ?>

    <?php // echo $form->field($model, 'Sector') ?>

    <?php // echo $form->field($model, 'Telefon') ?>

    <?php // echo $form->field($model, 'NrContract') ?>

    <?php // echo $form->field($model, 'DataContract') ?>

    <?php // echo $form->field($model, 'CasaDeSanatate') ?>

    <?php // echo $form->field($model, 'Impozitat') ?>

    <?php // echo $form->field($model, 'Pensionar') ?>

    <?php // echo $form->field($model, 'FaraContribCCI') ?>

    <?php // echo $form->field($model, 'FaraContribSanatateAngajator') ?>

    <?php // echo $form->field($model, 'FaraContribSanatateSalariat') ?>

    <?php // echo $form->field($model, 'TipPlata') ?>

    <?php // echo $form->field($model, 'CISerieNumar') ?>

    <?php // echo $form->field($model, 'CIEliberatDe') ?>

    <?php // echo $form->field($model, 'CIEliberatData') ?>

    <?php // echo $form->field($model, 'Email') ?>

    <?php // echo $form->field($model, 'NormaPeZiSpecific') ?>

    <?php // echo $form->field($model, 'OrePeLunaSpecific') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
