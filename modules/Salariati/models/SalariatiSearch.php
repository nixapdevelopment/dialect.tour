<?php

namespace app\modules\Salariati\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Salariati\models\Salariati;

/**
 * SalariatiSearch represents the model behind the search form about `app\modules\Salariati\models\Salariati`.
 */
class SalariatiSearch extends Salariati
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'PunctDeLucru', 'FunctieBaza', 'NormaPeZi', 'OrePeLuna', 'ContrSomajIndividuala', 'ContrSomajAngajator', 'ContrFGCS', 'ContrAccMunca', 'ZileCOAn', 'Judet', 'Pensionar', 'FaraContribCCI', 'FaraContribSanatateAngajator', 'FaraContribSanatateSalariat', 'NormaPeZiSpecific', 'OrePeLunaSpecific'], 'integer'],
            [['Nume', 'Prenume', 'Functie', 'DataAngajarii', 'Tip', 'CASIndividuala', 'CASAngajator', 'TipSalariu', 'CNP', 'Localitate', 'Strada', 'Numar', 'CodPostal', 'Bloc', 'Scara', 'Etaj', 'Apartament', 'Sector', 'Telefon', 'NrContract', 'DataContract', 'CasaDeSanatate', 'Impozitat', 'TipPlata', 'CISerieNumar', 'CIEliberatDe', 'CIEliberatData', 'Email'], 'safe'],
            [['Avans', 'SalariuBrut', 'SalariuOrar'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Salariati::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'PunctDeLucru' => $this->PunctDeLucru,
            'DataAngajarii' => $this->DataAngajarii,
            'FunctieBaza' => $this->FunctieBaza,
            'NormaPeZi' => $this->NormaPeZi,
            'OrePeLuna' => $this->OrePeLuna,
            'ContrSomajIndividuala' => $this->ContrSomajIndividuala,
            'ContrSomajAngajator' => $this->ContrSomajAngajator,
            'ContrFGCS' => $this->ContrFGCS,
            'ContrAccMunca' => $this->ContrAccMunca,
            'ZileCOAn' => $this->ZileCOAn,
            'Avans' => $this->Avans,
            'SalariuBrut' => $this->SalariuBrut,
            'SalariuOrar' => $this->SalariuOrar,
            'Judet' => $this->Judet,
            'DataContract' => $this->DataContract,
            'Pensionar' => $this->Pensionar,
            'FaraContribCCI' => $this->FaraContribCCI,
            'FaraContribSanatateAngajator' => $this->FaraContribSanatateAngajator,
            'FaraContribSanatateSalariat' => $this->FaraContribSanatateSalariat,
            'NormaPeZiSpecific' => $this->NormaPeZiSpecific,
            'OrePeLunaSpecific' => $this->OrePeLunaSpecific,
        ]);

        $query->andFilterWhere(['like', 'Nume', $this->Nume])
            ->andFilterWhere(['like', 'Prenume', $this->Prenume])
            ->andFilterWhere(['like', 'Functie', $this->Functie])
            ->andFilterWhere(['like', 'Tip', $this->Tip])
            ->andFilterWhere(['like', 'CASIndividuala', $this->CASIndividuala])
            ->andFilterWhere(['like', 'CASAngajator', $this->CASAngajator])
            ->andFilterWhere(['like', 'TipSalariu', $this->TipSalariu])
            ->andFilterWhere(['like', 'CNP', $this->CNP])
            ->andFilterWhere(['like', 'Localitate', $this->Localitate])
            ->andFilterWhere(['like', 'Strada', $this->Strada])
            ->andFilterWhere(['like', 'Numar', $this->Numar])
            ->andFilterWhere(['like', 'CodPostal', $this->CodPostal])
            ->andFilterWhere(['like', 'Bloc', $this->Bloc])
            ->andFilterWhere(['like', 'Scara', $this->Scara])
            ->andFilterWhere(['like', 'Etaj', $this->Etaj])
            ->andFilterWhere(['like', 'Apartament', $this->Apartament])
            ->andFilterWhere(['like', 'Sector', $this->Sector])
            ->andFilterWhere(['like', 'Telefon', $this->Telefon])
            ->andFilterWhere(['like', 'NrContract', $this->NrContract])
            ->andFilterWhere(['like', 'CasaDeSanatate', $this->CasaDeSanatate])
            ->andFilterWhere(['like', 'Impozitat', $this->Impozitat])
            ->andFilterWhere(['like', 'TipPlata', $this->TipPlata])
            ->andFilterWhere(['like', 'CISerieNumar', $this->CISerieNumar])
            ->andFilterWhere(['like', 'CIEliberatDe', $this->CIEliberatDe])
            ->andFilterWhere(['like', 'CIEliberatData', $this->CIEliberatData])
            ->andFilterWhere(['like', 'Email', $this->Email]);

        return $dataProvider;
    }
}
