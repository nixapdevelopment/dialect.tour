<?php

namespace app\modules\Tour\models;

use yii\base\Model;


class SearchFormModel extends Model
{
    
    public  $CityFrom,
            $CountryTo,
            $ZoneTo,
            $StationTo,
            $Hotel,
            $Date,
            $Duration,
            $Sort,
            $Rooms,
            $Adults = [],
            $Childs = [];

    public function rules()
    {
        return [
            [['Transport', 'CityFrom', 'CountryTo', 'ZoneTo', 'StationTo', 'Hotel', 'Date', 'Duration', 'Sort', 'Nights', 'Rooms', 'Adults', 'Childs'], 'required'],
            [['Transport', 'CityFrom', 'CountryTo', 'ZoneTo', 'StationTo', 'Hotel', 'Nights', 'Rooms'], 'integer'],
        ];
    }
    
}