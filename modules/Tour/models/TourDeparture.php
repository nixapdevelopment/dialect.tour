<?php

namespace app\modules\Tour\models;

use Yii;
use app\modules\Operator\Operator\models\Operator;
use app\modules\Tour\models\TourPackage;
use app\modules\Location\models\Location;

/**
 * This is the model class for table "TourDeparture".
 *
 * @property integer $ID
 * @property integer $TourPackageID
 * @property integer $OperatorLocationID
 * @property integer $OperatorDepartureID
 *
 * @property Operator $operatorLocation
 * @property TourPackage $tourPackage
 */
class TourDeparture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TourDeparture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TourPackageID', 'OperatorLocationID', 'OperatorDepartureID'], 'required'],
            [['TourPackageID', 'OperatorLocationID', 'OperatorDepartureID'], 'integer'],
            [['OperatorLocationID'], 'exist', 'skipOnError' => true, 'targetClass' => Operator::className(), 'targetAttribute' => ['OperatorLocationID' => 'ID']],
            [['TourPackageID'], 'exist', 'skipOnError' => true, 'targetClass' => TourPackage::className(), 'targetAttribute' => ['TourPackageID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TourPackageID' => Yii::t('app', 'Tour Package ID'),
            'OperatorLocationID' => Yii::t('app', 'Operator Location ID'),
            'OperatorDepartureID' => Yii::t('app', 'Operator Departure ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['ID' => 'LocationID'])->with('lang');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTourPackage()
    {
        return $this->hasOne(TourPackage::className(), ['ID' => 'TourPackageID']);
    }
    
}