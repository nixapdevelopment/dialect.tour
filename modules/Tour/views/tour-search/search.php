<?php

    /* @var $result app\modules\Tour\entities\Tour */

?>

<?php foreach ($results as $result) { ?>
    <div style="border-bottom: 1px solid #ccc;" class="sejur-item">
        <div class="row no-padding tour-row">
            <div class="col-md-3 text-left">
                <div class="row no-padding">
                    <div class="col-md-9">
                        <div class="hotel-name">
                            <?= $result->HotelName ?>
                            <div>
                                <?php for ($i = 1; $i <= $result->HotelStars; $i++) { ?>
                                <i style="color: #bba548" class="fa fa-star"></i>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <?php if (count($result->Offers) > 1) { ?>
                        <div class="pull-right">
                            <span style="cursor: pointer;" class="fa fa-plus more-info"></span>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center">
                <div style="font-size: 11px;" class="room-type">
                    <?= $result->firstOffer()->RoomName ?>
                </div>
            </div>
            <div style="font-size: 11px;" class="col-md-2 text-center">
                <?= is_array($result->firstOffer()->BoardName) ? $result->firstOffer()->BoardName[1] : $result->firstOffer()->BoardName ?>
            </div>
            <div class="col-md-1 text-center">
                <div style="position: relative; top: -5px;">
                    <div class="old-price">
                        <strike>EUR <?= $result->firstOffer()->OldPrice ?></strike>
                    </div>
                    <div class="new-price">
                        EUR <?= $result->firstOffer()->Price ?>
                    </div>
                </div>
            </div>
            <div class="col-md-1 text-center">
                <span class="fa fa-check-circle"></span>
            </div>
            <div class="col-md-1 text-center">
                <span class="fa fa-check-circle"></span>
            </div>
            <div class="col-md-1">
                <a data-hash="<?= $hash ?>" data-rate-key="<?= urlencode(serialize($result->firstOffer()->RateKeyData)) ?>" data-nr="0" class="reservations get-offer">
                    Detalii
                </a>
            </div>
        </div>
        <div style="display: none;" class="tour-price-results">
        <?php foreach ($result->Offers as $key => $offer) { ?>
            <?php if ($key == 0) continue; ?>
            <div class="row no-padding tour-row">
                <div class="col-md-3 text-left">
                    
                </div>
                <div class="col-md-3 text-center">
                    <div style="font-size: 11px;" class="room-type">
                        <?= $offer->RoomName ?>
                    </div>
                </div>
                <div style="font-size: 11px;" class="col-md-2 text-center">
                    <?= $offer->BoardName ?>
                </div>
                <div class="col-md-1 text-center">
                    <!--div>
                        <span class="fa fa-clock-o"></span>
                    </div-->
                    <div class="old-price">
                        <strike>EUR <?= $offer->OldPrice ?></strike>
                    </div>
                    <div class="new-price">
                        EUR <?= $offer->Price ?>
                    </div>
                </div>
                <div class="col-md-1 text-center">
                    <span class="fa fa-check-circle"></span>
                </div>
                <div class="col-md-1 text-center">
                    <span class="fa fa-check-circle"></span>
                </div>
                <div class="col-md-1">
                    <a data-hash="<?= $hash ?>" data-rate-key="<?= urlencode(serialize($offer->RateKeyData)) ?>" data-nr="<?= $key ?>" class="reservations get-offer">
                        Detalii
                    </a>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
<?php } ?>

<style>
    .row.no-padding.tour-row {
        padding: 5px;
    }
</style>