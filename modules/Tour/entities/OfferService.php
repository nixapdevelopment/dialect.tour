<?php

namespace app\modules\Tour\entities;


class OfferService
{
    
    /** 
     * Service type
     * @var string
     */
    public $Type;
    
    /** 
     * Service description
     * @var string
     */
    public $Name;
    
    /** 
     * Availability type (Immediate, OnRequest)
     * @var string
     */
    public $Availability;
    
    /** 
     * Service price
     * @var float
     */
    public $Price;
    
    
    
}