<?php

namespace app\modules\Tour\parsers\Paralela;

class RoomsConverter {
    
    public static function rooms()
    {
        return [
            'DB' => [
                'Name' => 'Double',
                'Adults' => 2,
                'Childs' => 0,
                'MaxAdults' => 2,
                'Code' => 'DB',
            ],
            'D2X' => [
                'Name' => 'Double with 2 children',
                'Adults' => 2,
                'Childs' => 2,
                'MaxAdults' => 2,
                'Code' => 'D2X',
            ],
            'DBP' => [
                'Name' => 'Dubla partaj',
                'Adults' => 2,
                'Childs' => 0,
                'MaxAdults' => 2,
                'Code' => 'DBP',
            ],
            'FR' => [
                'Name' => 'Family room',
                'Adults' => 2,
                'Childs' => 2,
                'MaxAdults' => 4,
                'Code' => 'FR',
            ],
            'Q' => [
                'Name' => 'Quad room',
                'Adults' => 2,
                'Childs' => 2,
                'MaxAdults' => 4,
                'Code' => 'Q',
            ],
            'SB' => [
                'Name' => 'Single room',
                'Adults' => 1,
                'Childs' => 0,
                'MaxAdults' => 1,
                'Code' => 'SB',
            ],
            'S2X' => [
                'Name' => 'Single room',
                'Adults' => 1,
                'Childs' => 1,
                'MaxAdults' => 1,
                'Code' => 'S2X',
            ],
            'TR' => [
                'Name' => 'Triple room',
                'Adults' => 3,
                'Childs' => 2,
                'MaxAdults' => 3,
                'Code' => 'TR',
            ],
            'TB' => [
                'Name' => 'Twin room',
                'Adults' => 2,
                'Childs' => 0,
                'MaxAdults' => 2,
                'Code' => 'TB',
            ],
            'TS' => [
                'Name' => 'Twin for sole use',
                'Adults' => 1,
                'Childs' => 0,
                'MaxAdults' => 1,
                'Code' => 'TS',
            ],
            'TSX' => [
                'Name' => 'Twin with 1 children',
                'Adults' => 1,
                'Childs' => 1,
                'MaxAdults' => 1,
                'Code' => 'TSX',
            ],
            'TBX' => [
                'Name' => 'Twin with 2 children',
                'Adults' => 1,
                'Childs' => 2,
                'MaxAdults' => 1,
                'Code' => 'TSX',
            ],
            'AP' => [
                'Name' => 'Apartaments',
                'Adults' => 4,
                'Childs' => 4,
                'MaxAdults' => 4,
                'Code' => 'AP',
            ],
        ];
    }
    
    public static function getRoom($adults, $childs = 0, $childsAge = [])
    {
        $rooms = self::rooms();
        
        if ($adults == 1)
        {
            if ($childs == 0)
            {
                return $rooms['TS']['Code'];
            }
            
            if ($childs == 1)
            {
                return $rooms['TSX']['Code'];
            }
            
            if ($childs == 2)
            {
                return $rooms['TBX']['Code'];
            }
            
            if ($childs > 2)
            {
                return $rooms['FR']['Code'];
            }
        }
        
        if ($adults == 2)
        {
            if ($childs == 0)
            {
                return $rooms['DB']['Code'];
            }
            
            if ($childs == 1)
            {
                return $rooms['D2X']['Code'];
            }
            
            if ($childs == 2)
            {
                return $rooms['D2X']['Code'];
            }
            
            if ($childs > 2)
            {
                return $rooms['FR']['Code'];
            }
        }
        
        if ($adults == 3)
        {
            if ($childs == 0)
            {
                return $rooms['TR']['Code'];
            }
            
            if ($childs == 1)
            {
                return $rooms['Q']['Code'];
            }
            
            if ($childs == 2)
            {
                return $rooms['FR']['Code'];
            }
        }
        
        if ($adults == 4)
        {
            if ($childs == 0)
            {
                return $rooms['Q']['Code'];
            }
            
            if ($childs == 1)
            {
                return $rooms['FR']['Code'];
            }
        }
        
        return $rooms['AP']['Code'];
    }
    
}