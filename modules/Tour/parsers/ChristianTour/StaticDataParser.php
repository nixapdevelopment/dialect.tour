<?php

namespace app\modules\Tour\parsers\ChristianTour;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\Location\models\Location;
use app\modules\Location\models\LocationLang;
use app\modules\Operator\Operator\models\OperatorLocation;
use app\modules\Tour\models\TourHotel;
use app\modules\Tour\models\TourPackage;
use app\modules\Tour\models\TourPackageDate;
use app\modules\Tour\models\TourDeparture;


class StaticDataParser
{
    
    public $operatorID = 6;

    public $endPoint = 'http://api.new.christiantour.ro/api/v1/static/';
    
    public $email = 'office@dialecttour.ro';
    
    public $password = 'AuVdKoPdQqHb2kbu';
    
    
    public function parse()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        
        //$this->parseLocations();
        //$this->parseHotels();
        $this->parsePackages();
    }
    
    private function parseLocations()
    {
        $params = http_build_query([
            'email' => $this->email,
            'password' => $this->password,
        ]);
        
        $requestUrl = $this->endPoint . 'destinations';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $response = curl_exec($ch);

        curl_close($ch);

        $results = json_decode($response, true);
        
        foreach ($results as $key => $val)
        {
            if ($val['what_api'] == 1)
            {
                unset($results[$key]);
            }
        }
        
        OperatorLocation::deleteAll(['OperatorID' => $this->operatorID]);
        
        $i = 0;
        $locationStack = [];
        foreach ($results as $result)
        {
            if (empty($result['type']))
            {
                continue;
            }
            
            switch ($result['type'])
            {
                case 'country':
                    $type = Location::TypeCountry;
                    break;
                case 'city':
                    $type = Location::TypeCity;
                    break;
                case 'area':
                    $type = Location::TypeArea;
                    break;
            }
            
            if (!empty($type))
            {
                $code = (int)$result['id'];
                $name = trim($result['content']['title']);
                
                $location = Location::find()->joinWith('lang')->where(['Name' => $name, 'Type' => $type])->one();

                if (empty($location->ID))
                {
                    $location = new Location();
                    $location->Type = $type;
                    $location->Latitude = 0;
                    $location->Longitude = 0;
                    $location->Code = NULL;
                    
                    if ($type == Location::TypeCity)
                    {
                        $location->CountryID = $locationStack[Location::TypeCountry][$result['country_id']];
                        $location->ParentID = $locationStack[Location::TypeCountry][$result['country_id']];
                    }
                    
                    if ($type == Location::TypeArea)
                    {
                        if (empty($locationStack[Location::TypeCity][$result['parent_id']]))
                        {
                            continue;
                        }
                        
                        $location->CountryID = $locationStack[Location::TypeCity][$result['parent_id']];
                        $location->ParentID = $locationStack[Location::TypeCity][$result['parent_id']];
                    }
                    
                    $location->save(false);

                    $locationLang = new LocationLang();
                    $locationLang->LangID = 'ro';
                    $locationLang->LocationID = $location->ID;
                    $locationLang->Name = $name;
                    $locationLang->save(false);
                }
                
                $locationStack[$type][$result['id']] = $location->ID;
                
                $operatorLocation = new OperatorLocation();
                $operatorLocation->OperatorID = $this->operatorID;
                $operatorLocation->LocationID = $location->ID;
                $operatorLocation->OperatorLocationID = $code;
                $operatorLocation->save(false);
            }
            
            $i++;
        }
        
        echo '<p>Locations: ' . count($results) . '</p>';
    }
    
    private function parseHotels()
    {
        $params = http_build_query([
            'email' => $this->email,
            'password' => $this->password,
        ]);
        
        $requestUrl = $this->endPoint . 'hotels';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $response = curl_exec($ch);

        curl_close($ch);

        $results = json_decode($response, true);

        if ($results)
        {
            TourHotel::deleteAll(['OperatorID' => $this->operatorID]);
            
            foreach ($results as $result)
            {
                $location = OperatorLocation::find()->where(['OperatorID' => $this->operatorID, 'OperatorLocationID' => $result['destination_id']])->limit(1)->one();
                
                if (!empty($location->ID))
                {
                    $tourHotel = new TourHotel();
                    $tourHotel->OperatorID = $this->operatorID;
                    $tourHotel->OperatorHotelID = $result['id'];
                    $tourHotel->LocationID = $location->LocationID;
                    $tourHotel->Name = $result['title'];
                    $tourHotel->Latitude = (float)$result['latitude'];
                    $tourHotel->Longitude = (float)$result['longitude'];
                    $tourHotel->Stars = (int)$result['class'];
                    $tourHotel->Description = '';
                    $tourHotel->Type = isset($result['types'][0]['title']) ? $result['types'][0]['title'] : 'Other';
                    
                    $roomsArr = [];
                    foreach ($result['rooms'] as $room)
                    {
                        $roomsArr[] = $room['room_content']['title'];
                    }
                    
                    $tourHotel->RoomTypes = implode('|', $roomsArr);
                    
                    $tourHotel->save(false);
                }
            }
        }
        
        echo '<p>Hotels: ' . count($results) . '</p>';
    }
    
    public function parsePackages()
    {
        $params = http_build_query([
            'email' => $this->email,
            'password' => $this->password,
        ]);
        
        $requestUrl = $this->endPoint . 'packages';
         
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $response = curl_exec($ch);

        curl_close($ch);

        $results = json_decode($response, true);
        
        TourPackage::deleteAll(['OperatorID' => $this->operatorID]);
        
        foreach ($results as $result)
        {
            $location = OperatorLocation::find()->where(['OperatorID' => $this->operatorID, 'OperatorLocationID' => $result['destination_id']])->limit(1)->one();
            
            if (!empty($location->ID))
            {
                $tourPackage = new TourPackage();
                $tourPackage->OperatorID = $this->operatorID;
                $tourPackage->OperatorPackageID = $result['id'];
                $tourPackage->HotelID = $result['hotel_id'];
                $tourPackage->LocationID = $location->LocationID;
                $tourPackage->TransportType = $result['transport_id'];
                $tourPackage->Duration = $result['duration'];
                $tourPackage->Currency = $result['currency'];
                $tourPackage->Name = $result['title'];
                $tourPackage->Description = '';
                $tourPackage->IsRecomended = $result['is_recommended'] == '0' ? 0 : 1;
                
                $extraArr = [];
                foreach ($result['extra_content'] as $item)
                {
                    $extraArr[] = [
                        'Title' => $item['title'],
                        'Content' => $item['content'],
                    ];
                }
                
                $tourPackage->Extra = serialize($extraArr);
                $tourPackage->save(false);
                
                foreach ($result['departure_dates'] as $date)
                {
                    $tourDate = new TourPackageDate();
                    $tourDate->TourPackageID = $tourPackage->ID;
                    $tourDate->Date = date('Y-m-d', strtotime($date['date']));
                    $tourDate->Sold = $date['sold'];
                    $tourDate->Visible = $date['visible'];
                    $tourDate->save(false);
                }
                
                $insert = [];
                foreach ($result['departure_points'] as $depPoint)
                {
                    $location = OperatorLocation::find()->where(['OperatorLocationID' => $depPoint['departure_point'], 'OperatorID' => $this->operatorID])->one();
                    
                    if (!empty($location->ID))
                    {
                        $insert[] = [
                            'ID' => null,
                            'TourPackageID' => $tourPackage->ID,
                            'OperatorLocationID' => $location->LocationID,
                            'OperatorDepartureID' => (int)$depPoint['id'],
                        ];
                    }
                }
                
                Yii::$app->db->createCommand()->batchInsert(TourDeparture::tableName(), (new TourDeparture())->attributes(), $insert)->execute();
            }
        }
        
        echo '<p>Tours: ' . count($results) . '</p>';
    }
    
}