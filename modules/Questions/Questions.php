<?php

namespace app\modules\Questions;

use app\components\Module\SiteModule;
/**
 * Questions module definition class
 */
class Questions extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Questions\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
