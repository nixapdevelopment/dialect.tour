<?php

namespace app\modules\Parser\components;

class RemoteImage
{
    
    public static function getImageUrl($url)
    {
        return empty($url) ? 'http://ccwc.org/wp-content/themes/ccwc-theme/images/no-image-available.png' : $url;
    }
    
}
