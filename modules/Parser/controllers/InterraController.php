<?php

namespace app\modules\Parser\controllers;

use Yii;
use yii\httpclient\Client;
use yii\web\Controller;
use app\modules\Parser\models\Tour;
use app\modules\Parser\models\TourDate;
use app\modules\Parser\models\TourLocation;

class InterraController extends Controller
{

    public function actionIndex($type)
    {
        $link = $type == 'Sejur' ? 'sejururi' : 'circuite';
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl("http://www.interra.ro/export/$link.xml")
            ->send();
        
        if ($response->isOk)
        {
            foreach ($response->data['offer'] as $tour)
            {
                echo '<pre>';
                print_r($response->data['offer']);
                exit;
                
                
                $model = new Tour();
                $model->ResourceID = '1';
                $model->ExternalID = $tour['id'];
                $model->Title = $tour['title'];
                $model->Subtitle = $tour['subtitle'];
                $model->Link = $tour['link'];
                $model->Price = round($tour['price'], 2);
                $model->Currency = $tour['currency'];
                $model->Days = $tour['days'];
                $model->IncludeTaxes = $tour['taxes_included'] == false ? 0 : 1;
                
                foreach ($tour['transport_options'] as $option)
                {
                    $option = (array)$option;
                    foreach ($option as $opt)
                    {
                        if ($opt == 'avion')
                        {
                            $model->Airplane = 1;
                        }
                        elseif ($opt == 'autocar')
                        {
                            $model->Bus = 1;
                        }
                        elseif ($opt == 'croaziera')
                        {
                            $model->Ship = 1;
                        }
                    }
                }
                
                $model->Description = $tour['description'];
                $model->HotelInfo = $tour['hotel_info'];
                $model->Itinerary = $tour['itinerary'];
                $model->UsefullInfo = $tour['usefull_info'];
                $model->OptionalTrips = $tour['optional_trips'];
                
                if (isset($tour['images']['image']) && is_array($tour['images']['image']))
                {
                    $model->Images = implode('|', $tour['images']['image']);
                }
                
                $model->Type = $type;
                
                $model->save(false);
                
                if (isset($tour['dates']['date']) && is_array($tour['dates']['date']))
                {
                    foreach ($tour['dates']['date'] as $date)
                    {
                        $datesModel = new TourDate();
                        $datesModel->TourID = $model->ID;
                        $datesModel->Date = date('y.m.d', strtotime($date));
                        $datesModel->save();
                    }
                }
                
                if (isset($tour['locations']['location']) && is_array($tour['locations']['location']))
                {
                    foreach ($tour['locations']['location'] as $location)
                    {
                        $locationModel = new TourLocation();
                        $locationModel->TourID = $model->ID;
                        $locationModel->Country = isset($location['country']) ? $location['country'] : '';
                        $locationModel->Location = isset($location['city']) ? $location['city'] : '';
                        $locationModel->save();
                    }
                }
            }
        }
    }

}
