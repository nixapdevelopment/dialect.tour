<?php

namespace app\modules\Parser\providers;

use app\modules\Parser\providers\Provider;
use yii\httpclient\Client;
use app\modules\Location\models\Location;
use app\modules\Location\models\LocationLang;
use app\modules\Operator\Operator\models\OperatorLocation;
use app\modules\Parser\entities\Hotel;
use app\modules\Parser\entities\HotelRoom;
use app\modules\Parser\entities\RoomBoard;
use app\modules\Parser\entities\RoomBoardPrice;
use app\modules\Parser\entities\CancellationPolicy;
use app\modules\Parser\components\RoomNameConverter;
use app\modules\Parser\components\RoomBoardConverter;

class WhlProvider extends Provider
{
    
    public $operatorID = 5;

    public $endPoint = 'http://towers.netstorming.net/kalima/call.php';
    
    public $company = 'dialect';
    public $login = 'xmluser';
    public $password = 'dialectxml';

    public function getLocations()
    {
        // get countries
        $timestamp = date('YmdHis');
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <envelope>
                    <header>
                        <actor>' . $this->company . '</actor>   
                        <user>' . $this->login . '</user>
                        <password>' . $this->password . '</password>
                        <version>1.6.1</version>
                        <timestamp>' . $timestamp . '</timestamp>
                    </header>
                    <query type="countries" product="hotel" />
                    <timestamp>' . $timestamp . '</timestamp>
                    <language>EN</language>
                </envelope>';
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl($this->endPoint)
            ->setContent($xml)
            ->send();
        
        if ($response->isOk)
        {
            OperatorLocation::deleteAll(['OperatorID' => $this->operatorID]);
            
            $countryCodes = [];
            foreach ($response->data['response']['countries']['country'] as $country)
            {
                $code = $country['code']['@attributes']['value'];
                $name = $country['names']['name'][2]['@attributes']['value'];
                
                $country = Location::find()->joinWith('lang')->where(['Name' => $name, 'Type' => Location::TypeCountry])->one();

                if (empty($country->ID))
                {
                    $country = new Location();
                    $country->Type = Location::TypeCountry;
                    $country->Latitude = 0;
                    $country->Longitude = 0;
                    $country->Code = $code;
                    $country->save(false);

                    $regionLang = new LocationLang();
                    $regionLang->LangID = 'ro';
                    $regionLang->LocationID = $country->ID;
                    $regionLang->Name = $name;
                    $regionLang->save(false);
                }
                
                $operatorLocation = new OperatorLocation();
                $operatorLocation->OperatorID = $this->operatorID;
                $operatorLocation->LocationID = $country->ID;
                $operatorLocation->OperatorLocationID = $code;
                $operatorLocation->save(false);
                
                $countryCodes[$country->ID] = $code;
            }
            
            foreach ($countryCodes as $countryID => $countryCode)
            {
                $timestamp = date('YmdHis');
                $cityXml = '<?xml version="1.0" encoding="UTF-8"?>
                            <envelope>
                                <header>
                                    <actor>' . $this->company . '</actor>   
                                    <user>' . $this->login . '</user>
                                    <password>' . $this->password . '</password>
                                    <version>1.6.1</version>
                                    <timestamp>' . $timestamp . '</timestamp>
                                </header>
                                <query type="cities" product="hotel">
                                    <country code="' . trim($countryCode) . '"/>
                                </query>
                            </envelope>';
                
                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('post')
                    ->setUrl($this->endPoint)
                    ->setContent($cityXml)
                    ->send();

                if ($response->isOk)
                {
                    if (!isset($response->data['response']['cities'])) continue;
                    
                    foreach ($response->data['response']['cities']['city'] as $city)
                    {
                        $code = trim($city['code']['@attributes']['value']);
                        $name = trim($city['names']['name'][2]['@attributes']['value']);
                        
                        if (empty($code) || empty($name)) continue;
                        
                        $city = Location::find()->joinWith('lang')->where(['Name' => $name])->limit(1)->one();
                        
                        if (empty($city->ID))
                        {
                            $city = new Location();
                            $city->Type = Location::TypeCity;
                            $city->CountryID = $countryID;
                            $city->ParentID = $countryID;
                            $city->Latitude = 0;
                            $city->Longitude = 0;
                            $city->Code = $code;
                            $city->save(false);
                            
                            $cityLang = new LocationLang();
                            $cityLang->LangID = 'ro';
                            $cityLang->LocationID = $city->ID;
                            $cityLang->Name = $name;
                            $cityLang->save(false);
                        }
                        
                        $operatorLocation = new OperatorLocation();
                        $operatorLocation->OperatorID = $this->operatorID;
                        $operatorLocation->LocationID = $city->ID;
                        $operatorLocation->OperatorLocationID = $code;
                        $operatorLocation->save(false);
                    }
                }
                else
                {
                    echo '<pre>';
                    print_r($response);
                    exit;
                }
            }
        }
        else
        {
            echo '<pre>';
            print_r($response);
        }
    }
    
    public function searchHotels($data, $hash)
    {
        $nights = (int)$data['Nights'];
        $plusTimeStr = $nights > 1 ? "+$nights days" : "+1 day";

        $checkIn = date('Y-m-d', strtotime($data['CheckIn']));
        $checkOut = date('Y-m-d', strtotime($plusTimeStr, strtotime($checkIn)));

        $rooms = (int)$data['Rooms'];
        
        $paxesStr = '';
        foreach ($data['Adults'] as $key => $nr)
        {
            $paxesStr .= '<room occupancy="' . $data['Adults'][$key] . '" required="1" extrabed="' . (isset($data['Childs'][$key]) && $data['Childs'][$key] > 0 ? 'true' : 'false') . '" ' . (isset($data['ChildAge'][$key]) && is_array($data['ChildAge'][$key]) ? ('age="' . implode('-', $data['ChildAge'][$key]) . '"') : false) . ' />';
        }

        $locationID = (int)$data['LocationID'];
        $operatorLocation = OperatorLocation::find()->with('location.lang')->where(['LocationID' => $locationID, 'OperatorID' => $this->operatorID])->limit(1)->one();
        
        $timestamp = date('YmdHis');
        $searchXml = '<?xml version="1.0" encoding="UTF-8"?>
                        <envelope>
                            <header>
                                <actor>' . $this->company . '</actor>
                                <user>' . $this->login . '</user>
                                <password>' . $this->password . '</password>
                                <version>1.6.1</version>
                                <timestamp>' . $timestamp . '</timestamp>
                            </header>
                            <query type="availability" product="hotel">
                                <nationality>RO</nationality>
                                <filters>
                                    <filter>AVAILONLY</filter>
                                </filters>
                                <checkin date="' . $checkIn . '"/>
                                <checkout date="' . $checkOut . '"/>
                                <city code="' . $operatorLocation->OperatorLocationID . '"/>
                                <details>
                                    ' . $paxesStr . '
                                </details>
                            </query>
                        </envelope>';
        
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl($this->endPoint)
            ->setContent($searchXml)
            ->send();
        
        if ($response->isOk)
        {
            $hotels = [];
            
            if (empty($response->data['response']['hotels']['hotel']))
            {
                return [];
            }
            
//            echo '<pre>';
//            print_r($response->data);
//            exit;
            
            $hotelClasses = [];
            foreach ($response->data['response']['hotels']['hotel'] as $hotel)
            {
//                echo '<pre>';
//                print_r($response->data['response']['hotels']['hotel']);
//                exit;
                
                $code = $hotel['@attributes']['code'];
                $name = $hotel['@attributes']['name'];
                $address = $hotel['@attributes']['address'];
                $stars = (int)$hotel['@attributes']['stars'];
                
                $agreements = [];
                foreach ($hotel['agreement'] as $agreement)
                {
                    $agreements[$agreement['room']['@attributes']['type']][$agreement['@attributes']['room_basis'] . $agreement['@attributes']['meal_basis'] . $agreement['@attributes']['total_gross']] = [
                        'id' => $agreement['@attributes']['id'],
                        'room_basis' => $agreement['@attributes']['room_basis'],
                        'meal_basis' => $agreement['@attributes']['meal_basis'],
                        'deadline' => $agreement['@attributes']['deadline'],
                        'total_gross' => $agreement['@attributes']['total_gross'],
                        'room' => $agreement['room']['@attributes']['type'],
                        'special' => $agreement['@attributes']['special'] == 'true',
                    ];
                }
                
                $hotels[] = [
                    'code' => $code,
                    'name' => $name,
                    'address' => $address,
                    'stars' => $stars,
                    'agreements' => $agreements,
                ];
            }
            
            $hotelIDs = [];
            foreach ($hotels as $hotel2)
            {
                $hotelIDs[] = $hotel2['code'];
                
                $hotelClass = new Hotel();
                $hotelClass->ExternalID = $hotel2['code'];
                $hotelClass->OperatorID = $this->operatorID;
                $hotelClass->Hash = $hash;
                $hotelClass->MainImage = '';
                $hotelClass->Stars = $hotel2['stars'];
                $hotelClass->Name = $hotel2['name'];
                $hotelClass->Address = $operatorLocation->location->lang->Name . ', ' . $hotel2['address'];
                $hotelClass->Latitude = '';
                $hotelClass->Longitude = '';
                $hotelClass->MinPrice = '';
                $hotelClass->Currency = 'EUR';
                $hotelClass->RoomInfo = '';
                $hotelClass->Email = '';
                $hotelClass->Images = [];
                $hotelClass->Rooms = [];
                $hotelClass->IsBest = false;
                $hotelClass->Description = '';
                $hotelClass->Reviews = [];
                
                $rooms = [];
                foreach ($hotel2['agreements'] as $roomType => $data)
                {
                    $roomClass = new HotelRoom();
                    $roomClass->Name = RoomNameConverter::convert($roomType);
                    $roomClass->Boards = [];
                    
                    foreach ($data as $d)
                    {
                        $roomBoard = new RoomBoard();
                        $roomBoard->Name = RoomBoardConverter::convert($d['room_basis']);
                        
                        $roomBoardPrices = new RoomBoardPrice();
                        $roomBoardPrices->Price = round($d['total_gross'], 2);
                        $roomBoardPrices->IsSuperDeal = !empty($d['special']);
                        $roomBoardPrices->Currency = 'EUR';
                        
                        $rateKeyData['rateKey'] = $d['id'];
                        $rateKeyData['boardCode'] = $d['room_basis'];
                        $rateKeyData['boardName'] = $roomBoard->Name;
                        $rateKeyData['paymentType'] = '';
                        $rateKeyData['price'] = $roomBoardPrices->Price;
                        $rateKeyData['roomCode'] = $roomType;
                        $rateKeyData['roomName'] = $roomClass->Name;
                        $rateKeyData['roomCount'] = $data['Rooms'];
                        
                        $roomBoardPrices->RateKeyData = $rateKeyData;
                        
                        $cancelationPolicyClass = new CancellationPolicy();
                        $cancelationPolicyClass->Percent = 100;
                        $cancelationPolicyClass->Date = date('d.m.Y H:i:s', strtotime($d['deadline']));
                        $cancelationPolicyClass->Description = '100% cancelatiion fees after ' . date('d.m.Y H:i', strtotime($d['deadline']));
                        
                        $roomBoardPrices->CancellationPolicies[] = $cancelationPolicyClass;
                        
                        $roomBoard->RoomBoardPrices[] = $roomBoardPrices;
                        
                        $roomClass->Boards[] = $roomBoard;
                    }
                    $hotelClass->Rooms[] = $roomClass;
                }
                
                $hotelClass->MinPrice = $hotelClass->getMinPrice();
                
                $hotelClasses[$this->operatorID . '-' . $hotelClass->ExternalID] = $hotelClass;
            }
            
            $timestamp = date('YmdHis');
            
            $hotelsTags = '';
            foreach ($hotelIDs as $hID)
            {
                $hotelsTags .= "<hotel id=\"$hID\"/>";
            }
            
            $hotelInfoXml = '<?xml version="1.0" encoding="UTF-8"?>
                            <envelope>
                                <header>
                                    <actor>' . $this->company . '</actor>
                                    <user>' . $this->login . '</user>
                                    <password>' . $this->password . '</password>
                                    <version>1.6.1</version>
                                    <timestamp>' . $timestamp . '</timestamp>
                                </header>
                                <query type="details" product="hotel">
                                    ' . $hotelsTags . '
                                </query>
                            </envelope>';
            
            
//            $client = new Client();
//            $response = $client->createRequest()
//            ->setMethod('post')
//            ->setUrl($this->endPoint)
//            ->setContent($hotelInfoXml)
//            ->send();
//        
//            if ($response->isOk)
//            {
//                echo '<pre>';
//                print_r($response->data);
//                exit;
//            }
            
            return $hotelClasses;
        }
        else
        {
            echo '<pre>';
            print_r($response);
            exit;
        }
    }
    
}