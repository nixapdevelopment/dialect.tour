<?php

namespace app\modules\Parser\entities;

class RoomBoardPrice
{
    
    /**
     * Room board price
     * @var float
     */
    public $Price;
    
    /**
     * Room board old price
     * @var float
     */
    public $PriceOld = 0;
    
    /**
     * Discount percent
     * @var float
     */
    public $DiscountPercent = 0;
    
    /**
     * Price is superdeal
     * @var bool
     */
    public $IsSuperDeal = false;
    
    /**
     * Price currency - 3 character currency ISO code
     * @var string
     */
    public $Currency;
    
    /**
     * Cancellation policies - array of CancellationPolicy classes
     * @var CancellationPolicy[]
     */
    public $CancellationPolicies = [];
    
    /**
     * Unique rate key data for identifying specific combination of dates, paxes, hotel, board type, etc...
     * @var string
     */
    public $RateKeyData = [];
    
    /**
     * @return string the room board cancelations info as string
     */
    public function getCancellationPoliciesString()
    {
        $arr = [];
        foreach ($this->CancellationPolicies as $policy)
        {
            $arr[] = $policy->Description;
        }
        
        if (count($arr) == 0)
        {
            return '<span style="margin-top: 2px;" class="fa fa-money pull-left"></span> No cancelation';
        }
        
        return '<span style="margin-top: 2px;" class="fa fa-money pull-left"></span> <div style="font-size: 13px;">' . implode('</div><br /><span style="margin-top: 2px;" class="fa fa-money pull-left"></span>  <div style="font-size: 13px;">', $arr) . '</div>';
    }
    
}