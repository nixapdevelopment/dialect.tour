<?php

namespace app\modules\Site;

use Yii;
use app\components\Module\SiteModule;

/**
 * Site module definition class
 */
class Site extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Site\controllers';
    
}
