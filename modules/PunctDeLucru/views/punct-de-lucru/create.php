<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\PunctDeLucru\models\PunctDeLucru */

$this->title = Yii::t('app', 'Create Punct De Lucru');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Punct De Lucrus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="punct-de-lucru-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
