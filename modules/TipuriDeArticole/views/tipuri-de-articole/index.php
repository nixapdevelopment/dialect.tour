<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\components\GridView\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\TipuriDeArticole\models\TipuriDeArticoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tipuri De Articole');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipuri-de-articole-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin([
        'id' => 'list-pjax',
    ]); ?>  
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'Denumire',
                'ContArticol',
                'Diferente',
                'TVANeexigibila',
                'Cheltuieli',
                'Venituri',
                [
                    'attribute' => 'Aprovizionabil',
                    'format' => 'raw',
                    'value' => function ($model, $index, $widget) {
                        return Html::tag('div', Html::checkbox('Aprovizionabil', $model->Aprovizionabil), ['class' => 'text-center']);
                    },
                ],
                [
                    'attribute' => 'Consumabil',
                    'format' => 'raw',
                    'value' => function ($model, $index, $widget) {
                        return Html::tag('div', Html::checkbox('Consumabil', $model->Consumabil), ['class' => 'text-center']);
                    },
                ],
                [
                    'attribute' => 'Vandabil',
                    'format' => 'raw',
                    'value' => function ($model, $index, $widget) {
                        return Html::tag('div', Html::checkbox('Vandabil', $model->Vandabil), ['class' => 'text-center']);
                    },
                ],
                [
                    'attribute' => 'Produs',
                    'format' => 'raw',
                    'value' => function ($model, $index, $widget) {
                        return Html::tag('div', Html::checkbox('Produs', $model->Produs), ['class' => 'text-center']);
                    },
                ],
                [
                    'attribute' => 'Nestocat',
                    'format' => 'raw',
                    'value' => function ($model, $index, $widget) {
                        return Html::tag('div', Html::checkbox('Nestocat', $model->Nestocat), ['class' => 'text-center']);
                    },
                ],

                [
                    'class' => 'app\components\GridView\ActionColumn',
                    'header' => Html::a(Yii::t('app', 'Adauga'), '#edit-modal', ['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-backdrop' => false, 'data-remote' => Url::to(['create'])]),
                    'buttons' => [
                        'update' => function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#edit-modal', [
                                'title' => 'Modifica', 
                                'data-toggle' => 'modal', 
                                'data-backdrop' => false, 
                                'data-remote' => $url,
                            ]);
                        },
                        'delete' => function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::current(['delete' => $model->ID]), [
                                'title' => 'Sterge',
                                'onclick' => 'return confirm("' . Yii::t('app', 'Sterge?') . '");',
                                'data-pjax' => '0'
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>

<?php Modal::begin([
    'id' => 'edit-modal',
    'size' => Modal::SIZE_LARGE,
    'header' => Html::tag('h4', Yii::t('app', 'Editarea tipul de articol')),
]) ?>
<?php Modal::end() ?>

<?php $this->registerJs("
    $(document).on('click','[data-remote]', function(e) {
        e.preventDefault();
        $('#edit-modal .modal-body').load($(this).data('remote'));
    });
    $(document).on('pjax:success', '#edit-form-wrap', function() {
        $.pjax.reload({container: '#list-pjax'});
        $('#edit-modal').modal('hide');
    });
"); ?>
