<?php

namespace app\modules\TipuriDeArticole\controllers;

use Yii;
use app\modules\TipuriDeArticole\models\TipuriDeArticole;
use app\modules\TipuriDeArticole\models\TipuriDeArticoleSearch;
use app\controllers\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TipuriDeArticoleController implements the CRUD actions for TipuriDeArticole model.
 */
class TipuriDeArticoleController extends BackendController
{

    /**
     * Lists all TipuriDeArticole models.
     * @return mixed
     */
    public function actionIndex()
    {
        if ($deleteID = (int)Yii::$app->request->get('delete', 0))
        {
            $this->findModel($deleteID)->delete();
            return $this->redirect(['index']);
        }
        
        $searchModel = new TipuriDeArticoleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TipuriDeArticole model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TipuriDeArticole model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TipuriDeArticole();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            
        }
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TipuriDeArticole model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            
        }
        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TipuriDeArticole model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TipuriDeArticole model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TipuriDeArticole the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TipuriDeArticole::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
