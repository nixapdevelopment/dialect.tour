<?php

namespace app\modules\Location\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Location\models\Location;

/**
 * LocationSearch represents the model behind the search form about `app\modules\Location\models\Location`.
 */
class LocationSearch extends Location
{
    /**
     * @inheritdoc
     */



    public function rules()
    {
        return [
            [['ID', 'ParentID','CountryID'], 'integer'],
            [['Country','Region','City'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Location::find()->joinWith(['lang']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if ($this->Country || Yii::$app->request->get('sort')){

        }
        $dataProvider->sort->attributes['Country'] = [
            'asc'  => ['LocationLang.Name' => SORT_ASC],
            'desc' => ['LocationLang.Name' => SORT_DESC],
        ];
        if ($this->Country) {

            $query->andFilterWhere(['like', 'CountryID', $this->Country])->with('lang');

        }
        if ($this->ParentID) {

            $query->andFilterWhere(['like', 'ParentID', $this->ParentID])->with('lang');

        }

        if ($this->City || Yii::$app->request->get('sort')) {

            $query->andFilterWhere(['like', 'LocationLang.Name', $this->City])->with('lang');
        }
        $dataProvider->sort->attributes['City'] = [
            'asc'  => ['LocationLang.Name' => SORT_ASC],
            'desc' => ['LocationLang.Name' => SORT_DESC],
        ];
            // grid filtering conditions
        $query->andFilterWhere([

            'Type' => $this->Type,

        ]);

        return $dataProvider;
    }
}
