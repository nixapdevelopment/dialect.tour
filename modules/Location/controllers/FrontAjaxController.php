<?php

namespace app\modules\Location\controllers;

use Yii;
use yii\web\Response;
use app\modules\Location\models\Location;

class FrontAjaxController extends \app\controllers\FrontController
{

    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionAjaxLocationsSearch($q)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $locations = Location::find()->joinWith('lang')->with('country.lang')->where(['like', 'Name', $q])->andWhere(['LangID' => 'ro'])->andWhere(['Type' => Location::TypeCity])->groupBy('LocationID')->all();
        
        $out = ['results' => []];
        foreach ($locations as $location)
        {
            switch ($location->Type)
            {
                case Location::TypeCountry:
                    $text = $location->lang->Name; 
                    break;
                case Location::TypeRegion:
                    $text = $location->lang->Name . ' (regiunea)'; 
                    break;
                case Location::TypeCity:
                    $text = $location->lang->Name . ' (' . $location->country->lang->Name . ')'; 
                    break;
            }
            
            $out['results'][] = [
                'id' => $location->ID,
                'text' =>  $text,
            ];
        }

        return $out;
    }

}