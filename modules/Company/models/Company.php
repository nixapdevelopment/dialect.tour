<?php

namespace app\modules\Company\models;

use Yii;
use app\modules\Judet\models\Judet;
use app\modules\User\models\UserCompany;

/**
 * This is the model class for table "Company".
 *
 * @property integer $ID
 * @property string $Denumire
 * @property string $CodFiscal
 * @property string $NrRegistrulComertului
 * @property string $CodCAEN
 * @property integer $Judet
 * @property string $Localitate
 * @property string $Sector
 * @property string $Strada
 * @property string $Numar
 * @property string $CodPostal
 * @property string $Bloc
 * @property string $Scara
 * @property string $Etaj
 * @property string $Apartament
 * @property string $Telefon
 * @property string $Email
 * @property integer $OperatiiInValuta
 * @property string $MetodaDeIesireStocuri
 * @property string $ModulDePlataTVA
 * @property integer $TVAColectataLaIncasare
 * @property integer $PersoanaJuridicaFaraScopLucrativ
 * @property integer $Microintreprindere
 * @property string $CapitalSocial
 * @property string $TipDeCalculAlDateiScadenteiInFacturi
 *
 * @property Judet $judet
 * @property UserCompany[] $userCompanies
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Denumire', 'CodFiscal', 'NrRegistrulComertului', 'CodCAEN', 'Judet', 'Localitate', 'MetodaDeIesireStocuri', 'ModulDePlataTVA', 'CapitalSocial'], 'required'],
            [['Judet', 'OperatiiInValuta', 'TVAColectataLaIncasare', 'PersoanaJuridicaFaraScopLucrativ', 'Microintreprindere'], 'integer'],
            [['CapitalSocial'], 'number'],
            [['Email'], 'email'],
            [['Denumire', 'CodFiscal', 'NrRegistrulComertului', 'Localitate', 'Sector', 'Strada', 'CodPostal', 'Bloc', 'Scara', 'Etaj', 'Apartament', 'Telefon', 'Email'], 'string', 'max' => 255],
            [['CodCAEN', 'Numar'], 'string', 'max' => 50],
            [['MetodaDeIesireStocuri', 'ModulDePlataTVA', 'TipDeCalculAlDateiScadenteiInFacturi'], 'string', 'max' => 20],
            [['Judet'], 'exist', 'skipOnError' => true, 'targetClass' => Judet::className(), 'targetAttribute' => ['Judet' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Denumire' => Yii::t('app', 'Denumire'),
            'CodFiscal' => Yii::t('app', 'Cod Fiscal'),
            'NrRegistrulComertului' => Yii::t('app', 'Nr. Registrul Comertului'),
            'CodCAEN' => Yii::t('app', 'Cod CAEN'),
            'Judet' => Yii::t('app', 'Judet'),
            'Localitate' => Yii::t('app', 'Localitate'),
            'Sector' => Yii::t('app', 'Sector'),
            'Strada' => Yii::t('app', 'Strada'),
            'Numar' => Yii::t('app', 'Numar'),
            'CodPostal' => Yii::t('app', 'Cod Postal'),
            'Bloc' => Yii::t('app', 'Bloc'),
            'Scara' => Yii::t('app', 'Scara'),
            'Etaj' => Yii::t('app', 'Etaj'),
            'Apartament' => Yii::t('app', 'Apartament'),
            'Telefon' => Yii::t('app', 'Telefon'),
            'Email' => Yii::t('app', 'Email'),
            'OperatiiInValuta' => Yii::t('app', 'Operatii In Valuta'),
            'MetodaDeIesireStocuri' => Yii::t('app', 'Metoda De Iesire Stocuri'),
            'ModulDePlataTVA' => Yii::t('app', 'Modul De Plata TVA'),
            'TVAColectataLaIncasare' => Yii::t('app', 'TVA colectata La Incasare'),
            'PersoanaJuridicaFaraScopLucrativ' => Yii::t('app', 'Persoana Juridica Fara Scop Lucrativ'),
            'Microintreprindere' => Yii::t('app', 'Microintreprindere'),
            'CapitalSocial' => Yii::t('app', 'Capital Social'),
            'TipDeCalculAlDateiScadenteiInFacturi' => Yii::t('app', 'Tip De Calcul Al Datei Scadentei In Facturi'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudet()
    {
        return $this->hasOne(Judet::className(), ['ID' => 'Judet']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCompanies()
    {
        return $this->hasMany(UserCompany::className(), ['CompanyID' => 'ID']);
    }
    
}