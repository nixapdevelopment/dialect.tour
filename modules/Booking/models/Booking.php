<?php

namespace app\modules\Booking\models;

use Yii;
use app\modules\Booking\models\BookingClient;
use app\modules\Booking\models\BookingQuery;

/**
 * This is the model class for table "Booking".
 *
 * @property integer $ID
 * @property string $Key
 * @property string $RateKey
 * @property string $Date
 * @property string $Type
 * @property string $Status
 *
 * @property BookingClient[] $bookingClients
 * @property BookingInfo[] $bookingInfos
 */
class Booking extends \yii\db\ActiveRecord
{
    
    const TypeHotel = 'Hotel';
    const TypeTourAvion = 'TourAvion';
    const TypeTourBus = 'TourBus';
    
    const StatusCreated = 'Created';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OperatorID', 'Key', 'RateKey', 'Date', 'Type', 'Status'], 'required'],
            [['OperatorID'], 'integer'],
            [['Key', 'RateKey'], 'string'],
            [['Hash'], 'default', 'value' => Yii::$app->session->get('searchHash')],
            [['Date'], 'safe'],
            [['Type', 'Status'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Key' => Yii::t('app', 'Key'),
            'RateKey' => Yii::t('app', 'Rate Key'),
            'Date' => Yii::t('app', 'Date'),
            'Type' => Yii::t('app', 'Type'),
            'Status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingClient()
    {
        return $this->hasOne(BookingClient::className(), ['BookingID' => 'ID'])->where(['Leading' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperator()
    {
        return $this->hasOne(BookingClient::className(), ['ID' => 'OperatorID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingClients()
    {
        return $this->hasMany(BookingClient::className(), ['BookingID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingInfos()
    {
        return $this->hasMany(BookingInfo::className(), ['BookingID' => 'ID']);
    }

    /**
     * @inheritdoc
     * @return BookingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookingQuery(get_called_class());
    }
}
