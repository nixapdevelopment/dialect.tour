<?php

namespace app\modules\Booking\models;

use Yii;
use app\modules\Booking\models\Booking;
use app\modules\Booking\models\BookingClientQuery;

/**
 * This is the model class for table "BookingClient".
 *
 * @property integer $ID
 * @property integer $BookingID
 * @property string $FirstName
 * @property string $LastName
 * @property string $ExtraData
 *
 * @property Booking $booking
 */
class BookingClient extends \yii\db\ActiveRecord
{
    const TypeAdult = 'Adult';
    const TypeChild = 'Child';
    const TypeInfant = 'Infant';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BookingClient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Leading'], 'default', 'value' => 0],
            [['Type'], 'default', 'value' => self::TypeAdult],
            [['BookingID', 'FirstName', 'LastName', 'Leading', 'Type'], 'required'],
            [['BookingID', 'Leading'], 'integer'],
            [['ExtraData'], 'string'],
            [['Type'], 'string', 'max' => 50],
            [['FirstName', 'LastName'], 'string', 'max' => 255],
            [['BookingID'], 'exist', 'skipOnError' => true, 'targetClass' => Booking::className(), 'targetAttribute' => ['BookingID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'BookingID' => Yii::t('app', 'Booking ID'),
            'FirstName' => Yii::t('app', 'Nume'),
            'LastName' => Yii::t('app', 'Prenume'),
            'Type' => Yii::t('app', 'Type'),
            'Leading' => Yii::t('app', 'Leading'),
            'ExtraData' => Yii::t('app', 'Extra Data'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['ID' => 'BookingID']);
    }

    /**
     * @inheritdoc
     * @return BookingClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookingClientQuery(get_called_class());
    }
}
