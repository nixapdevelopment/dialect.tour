<?php

    use app\views\themes\front\assets\FrontAsset;
    use yii\widgets\ActiveForm;
    
    $bundle = FrontAsset::register($this);
    
    /* @var $hotel app\modules\Parser\entities\Hotel */
    /* @var $leadingClient app\modules\Booking\models\BookingClient */

?>
<br />
<section class="pay">
    <div class="container">
        <div class="tur-item">
            <div class="row">
                <div class="col-md-2">
                    <div class="img-tur">
                        <img class="img-responsive" src="<?= $hotel->MainImage ?>" alt="<?= $hotel->Name ?>">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="info-tur-item">
                        <span class="tur-name"><?= $hotel->Name ?></span>
                        <span class="hotel-star">
                            <?php for ($i = 1; $i <= $hotel->Stars; $i++) { ?>
                            <i style="color: #bba548" class="fa fa-star"></i>
                            <?php } ?>
                        </span>
                        <div class="tur-location">
                            <?= $hotel->Address ?>
                        </div>
                        <div class="from-to">
                            <span class="strong">
                                From
                            </span>
                            <?= $searchData['CheckIn'] ?>
                            <span class="strong">
                                To
                            </span>
                            <?= $searchData['CheckOut'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <!--div class="review-clients">
                        <div class="pay-to">
                            <span class="fa fa-bed"></span>
                            Pay to HotelBads
                        </div>
                        <div>
                            <img class="img-responsive" src="<?= $bundle->baseUrl ?>/images/dots-review-five.png" alt="">
                            of 519 reviews
                        </div>
                        <div class="tur-price">
                            500.00 <sup>EURO</sup>
                        </div>
                    </div-->
                </div>
            </div>
            <div class="review-info-product">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <div class="total-products">
                                <?= $searchData['Rooms'] ?> X <?= $rateKeyData['roomName'] ?>, <?= $rateKeyData['boardName'] ?>, <?= array_sum($searchData['Adults']) ?> Adults, <?= array_sum((array)$searchData['Childs']) ?> Childrens
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-right">
                                <div class="total-price-finish">
                                    Total net amout <span class="strong">
                                        <?= $rateKeyData['price'] ?> <sup>EURO</sup>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-for-pay">
                <?php $form = ActiveForm::begin() ?>
                    <div class="type-info">
                        Leading person details
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($leadingClient, '[Leading]FirstName')->textInput([
                                'placeholder' => $leadingClient->getAttributeLabel('FirstName'),
                                'class' => '',
                            ]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($leadingClient, '[Leading]LastName')->textInput([
                                'placeholder' => $leadingClient->getAttributeLabel('LastName'),
                                'class' => '',
                            ]) ?>
                        </div>
                    </div>
                    <div>
                        <div class="type-info">
                            Accomodation details
                        </div>
                        <div class="row">
                            <?php foreach ($paxes as $roomNr => $roomPaxes) { ?>
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Room <?= $roomNr ?></b>
                                    </div>
                                    <div class="panel-body">
                                        <?php foreach ($roomPaxes as $key => $pax) { ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= $form->field($pax, "[$roomNr-$key]FirstName")->textInput([
                                                    'placeholder' => $pax->Type,
                                                    'class' => '',
                                                ]) ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($pax, "[$roomNr-$key]LastName")->textInput([
                                                    'placeholder' => $pax->Type,
                                                    'class' => '',
                                                ]) ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div>
                        <div class="type-info">
                            Booking details
                        </div>
                        <label>
                            Reference number
                        </label>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <input type="text" placeholder="Reference number">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="booking-info">
                        <!--div class="type-info">
                            Booking confirmation
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-left">
                                    Services
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-right">
                                    Net Price
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <span class="tur-name">Best Roma</span> -
                                4 STARS
                            </div>
                            <div class="col-md-6">
                                <div class="text-right pt10">
                                    <?= $rateKeyData['price'] ?> <sup>EURO</sup>
                                </div>
                            </div>
                        </div>
                        <hr-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-right">
                                    <div class="total-price-finish">
                                        Total net amout
                                        <span class="strong">
                                            <?= $rateKeyData['price'] ?> <sup>EURO</sup>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <div class="text-left mt10">
                                    <div>
                                        <label>
                                            <input required type="checkbox" name="read-terms">
                                            I have read and accept the <a href="#">general terms</a>
                                            and <a href="#">cancellation policy conditions</a>
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            Payment type
                                        </label>
                                        <select required name="type-payment" class="style-select">
                                            <option value="" selected>
                                                Select type
                                            </option>
                                            <option value="Visa">
                                                Visa Card
                                            </option>
                                            <option value="MasterCard">
                                                Master Card
                                            </option>
                                            <option value="PayPal">
                                                PayPal
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-default mt10">
                                        PAY NOW
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php ActiveForm::end() ?>
            </div>

        </div>
    </div>
</section>