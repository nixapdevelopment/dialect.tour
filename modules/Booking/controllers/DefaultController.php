<?php

namespace app\modules\Booking\controllers;

use Yii;
use yii\base\Model;
use app\modules\Booking\models\Booking;
use app\modules\Booking\models\BookingClient;

/**
 * Default controller for the `booking` module
 */
class DefaultController extends \app\controllers\FrontController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    
    public function init()
    {
        parent::init();
        
        $this->module->layoutPath = Yii::getAlias('@app/views/themes/front/layouts');
        $this->layout = 'frontend';
    }


    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionTryBooking($key, $rateKey, $type = Booking::TypeHotel)
    {
        $arrKey = explode('-', $key);
        
        $booking = new Booking();
        $booking->OperatorID = $arrKey[0];
        $booking->Key = $key;
        $booking->RateKey = urldecode($rateKey);
        $booking->Date = date('c');
        $booking->Type = $type;
        $booking->Status = Booking::StatusCreated;
        $booking->save();
        
        Yii::$app->session->set('__booking__', $booking->ID);
        
        $method = $type == Booking::TypeHotel ? 'confirm-hotel-booking' : 'confirm-tour-booking';
        
        return $this->redirect([$method]);
    }
    
    public function actionConfirmHotelBooking()
    {
        $bookingID = Yii::$app->session->get('__booking__');
        $booking = Booking::findOne($bookingID);
        
        $hash = Yii::$app->session->get('searchHash');
        
        $cacheFile = Yii::getAlias('@app/modules/HotelSearch/cache/' . $hash . '.txt');
        $results = unserialize(file_get_contents($cacheFile));
        
        $hotel = $results[$booking->Key];
        $rateKeyData = unserialize($booking->RateKey);
        
        $searchData = Yii::$app->session->get('searchData');
        
        $leadingClient = new BookingClient(['Type' => BookingClient::TypeAdult, 'Leading' => 1, 'BookingID' => $bookingID]);
        
        $paxes = [];
        for ($i = 1; $i <= $searchData['Rooms']; $i++)
        {
            for ($j = 1; $j <= $searchData['Adults'][$i]; $j++)
            {
                $paxes[$i][] = new BookingClient([
                    'BookingID' => $bookingID,
                    'Type' => BookingClient::TypeAdult,
                    'Leading' => 0,
                ]);
            }
            
            if (!empty($searchData['Adults'][$i]))
            {
                for ($j = 1; $j <= $searchData['Childs'][$i]; $j++)
                {
                    $paxes[$i][] = new BookingClient([
                        'BookingID' => $bookingID,
                        'Type' => BookingClient::TypeChild,
                        'Leading' => 0,
                        'ExtraData' => serialize([
                            'age' => $searchData['ChildAge'][$i][$j],
                        ]),
                    ]);
                }
            }
        }
        
        if (Yii::$app->request->isPost)
        {
            $data = Yii::$app->request->post();
            
            $leadingClient->FirstName = $data['BookingClient']['Leading']['FirstName'];
            $leadingClient->LastName = $data['BookingClient']['Leading']['LastName'];
            $leadingClient->BookingID = $bookingID;
            
            unset($data['BookingClient']['Leading']);

            $paxesModels = [];
            foreach ($paxes as $i => $item)
            {
                foreach ($item as $j => $pax)
                {
                    $paxesModels[$i . '-' . $j] = $pax;
                }
            }
            
            if ($leadingClient->validate() && Model::loadMultiple($paxesModels, $data) && Model::validateMultiple($paxesModels))
            {
                $leadingClient->save(false);
                
                foreach ($paxesModels as $model)
                {
                    $model->save(false);
                }
                
                return $this->redirect('success');
            }
        }
        
        return $this->render('confirm-hotel-booking', [
            'bookingID' => Yii::$app->session->get('__booking__'),
            'hotel' => $hotel,
            'rateKeyData' => $rateKeyData,
            'searchData' => $searchData,
            'leadingClient' => $leadingClient,
            'paxes' => $paxes,
        ]);
    }
    
    public function actionSuccess()
    {
        $bookingID = Yii::$app->session->get('__booking__');
        
        if (empty($bookingID))
        {
            return $this->goHome();
        }
        
        $booking = Booking::findOne($bookingID);
        
        Yii::$app->session->remove('__booking__');
        
        $hash = Yii::$app->session->get('searchHash');
        Yii::$app->session->remove('searchHash');
        
        $cacheFile = Yii::getAlias('@app/modules/HotelSearch/cache/' . $hash . '.txt');
        $results = unserialize(file_get_contents($cacheFile));
        
        $hotel = $results[$booking->Key];
        $rateKeyData = unserialize($booking->RateKey);
        
        $searchData = Yii::$app->session->get('searchData');
        Yii::$app->session->remove('searchData');
        
        return $this->render('success', [
            'hotel' => $hotel,
            'rateKeyData' => $rateKeyData,
            'searchData' => $searchData,
        ]);
    }
    
}
