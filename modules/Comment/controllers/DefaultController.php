<?php

namespace app\modules\Comment\controllers;

use app\controllers\SiteBackendController;

/**
 * Default controller for the `Comment` module
 */
class DefaultController extends SiteBackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
