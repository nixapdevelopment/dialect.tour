<div id="sw-<?= md5($hotelKey) ?>" style="max-height: 210px;" class="swiper-container gallery-tur-item">
    <!-- Slider -->
    <div class="swiper-wrapper">
        <!-- Slides -->
        <?php foreach ($images as $image) { ?>
        <div class="swiper-slide">
            <a style="width: 212px;overflow: hidden;display: block;" href="<?= $image ?>" data-lightbox="roadtrip<?= md5($hotelKey) ?>">
                <img style="border: 2px;" src="<?= $image ?>" alt="Hotel image">
            </a>
        </div>
        <?php } ?>
    </div>
    <!-- Pagination -->
    <div class="swiper-pagination"></div>
</div>

<script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 50,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
    });
    swiper.onSlideChangeStart = function(){
        $('#sw-<?= md5($hotelKey) ?>').height( $(swiper.activeSlide()).height() )
    };
</script>