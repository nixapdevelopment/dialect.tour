<?php

    use yii\helpers\Url;
    
    /* @var $result app\modules\Parser\entities\Hotel */

?>

<div>
    <h4 class="text-center"><?= count($results) ?> hoteluri desponibile</h4>
</div>
<div id="pager-wrap">
    <?php foreach ($results as $key => $result) { ?>
    <div class="row page-item">
        <div class="tur-item">
            <div class="row">
                <div class="col-md-3">
                    <div class="img-tur">
                        <img class="img-responsive" src="<?= $result->getMainImage() ?>" alt="<?= $result->Name ?>">
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="info-tur-item">
                                <span class="tur-name"><?= $result->Name ?></span>
                                <span class="hotel-star">
                                </span>
                                <div class="tur-location">
                                    <?= $result->Address ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="review-clients text-right">
                                <div>
                                    <?php for ($i = 1; $i <= $result->Stars; $i++) { ?>
                                    <i style="color: #bba548" class="fa fa-star"></i>
                                    <?php } ?>
                                </div>
                                <div class="tur-price">
                                    <?= $result->MinPrice ?> <sup>EUR</sup>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="toggle-group-button">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#description<?= md5($key) ?>" aria-controls="description<?= md5($key) ?>" role="tab" data-toggle="tab">
                                                    Description
                                                </a>
                                            </li>
                                            <li role="presentation">
                                                <a onclick="getHotelImages($(this).closest('.page-item').find('.toggle-gallery-tur-item'), '<?= $key ?>')" href="#gallery<?= md5($key) ?>" aria-controls="gallery<?= md5($key) ?>" role="tab" data-toggle="tab">
                                                    Images
                                                </a>
                                            </li>
                                            <li role="presentation">
                                                <a onclick="getHotelMap($(this).closest('.page-item').find('.hotel-map-tur'), '<?= $key ?>')" href="#map<?= md5($key) ?>" aria-controls="map<?= md5($key) ?>" role="tab" data-toggle="tab">
                                                    Map
                                                </a>
                                            </li>
                                            <li role="presentation" class="pull-right">
                                                <a style="margin-right: 15px;" onclick="getHotelPrices($(this).closest('.page-item').find('.toggle-price-bads'))" href="#price-and-beds<?= md5($key) ?>" aria-controls="price-and-beds<?= md5($key) ?>" role="tab" data-toggle="tab">
                                                    View more price & bads
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="description<?= md5($key) ?>">
                    <div class="toggle-description-tur-item">
                        <?= empty($result->Description) ? 'No description' : $result->Description ?>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="gallery<?= md5($key) ?>">
                    <div class="toggle-gallery-tur-item">
                        <!-- Hotel images via ajax -->
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="map<?= md5($key) ?>">
                    <div class="hotel-map-tur" id="hotel-map1">
                        <!-- Hotel map via ajax -->
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="price-and-beds<?= md5($key) ?>">
                    <div class="toggle-price-bads">
                        <?php foreach ($result->Rooms as $room) { ?>
                        <div class="type-room-and-board">
                            <div>
                                <div style="background-color: #e8e8e8;" class="white-bg">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="text-left room-type">
                                                <?= $room->Name ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div style="padding-right: 10px;" class="text-right operator-name">
                                                Hotelbeds
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <?php foreach ($room->Boards as $roomBoard) { ?>
                                    <div class="row">
                                        <div style="line-height: 45px; font-size: 12px;" class="col-md-2">
                                            <?= $roomBoard->Name ?>
                                        </div>
                                        <div class="col-md-10">
                                            <?php foreach ($roomBoard->RoomBoardPrices as $roomBoardPrice) { ?>
                                            <div class="type-board">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="text-left">
                                                            <ul>
                                                                <li>
                                                                    <?= $roomBoardPrice->getCancellationPoliciesString() ?>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-7 text-right">
                                                                <?php if ($roomBoardPrice->PriceOld) { ?>
                                                                <div class="old-price">
                                                                    <?= $roomBoardPrice->PriceOld ?> <?= $roomBoardPrice->Currency ?>
                                                                    <span class="line-middle"></span>
                                                                </div>
                                                                <?php } else { ?>
                                                                <div style="height: 10px;"></div>
                                                                <?php } ?>
                                                                <div class="new-price text-right">
                                                                    <?= $roomBoardPrice->Price ?> <?= $roomBoardPrice->Currency ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5 text-left">
                                                                <a href="<?= Url::to(['/booking/default/try-booking', 'key' => $key, 'rateKey' => urlencode(serialize($roomBoardPrice->RateKeyData))]) ?>" class="reservation">
                                                                    Rezerva
                                                                </a>
                                                            </div>
                                                            <?php if ($roomBoardPrice->DiscountPercent > 0) { ?>
                                                            <div class="sale">
                                                                -<?= $roomBoardPrice->DiscountPercent ?>%
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<script>
    setTimeout(function(){
        $('[data-toggle="tooltip"]').tooltip({
            html: true
        });
    }, 1000);
    
    setTimeout(function(){
        $('[data-toggle="tooltip"]').tooltip({
            html: true
        });
    }, 5000)
    
    function getHotelImages(imgWrap, hotelKey)
    {
        $(imgWrap).closest('.page-item').find('.hotel-info').hide();
        $(imgWrap).show();
        
        $.get('<?= Url::to(['/hotel-search/hotel-search/get-hotel-images']) ?>', {hotelKey: hotelKey}, function(html){
            $(imgWrap).html(html);
        });
    }
    
    function getHotelPrices(pricesWrap)
    {
        $(pricesWrap).closest('.page-item').find('.hotel-info').hide();
        $(pricesWrap).show();
    }
    
    function getHotelMap(mapWrap, hotelKey)
    {
        $(mapWrap).closest('.page-item').find('.hotel-info').hide();
        
        if ($(mapWrap).find('iframe').length == 0)
        {
            $.get('<?= Url::to(['/hotel-search/hotel-search/get-hotel-map']) ?>', {hotelKey: hotelKey}, function(html){
                $(mapWrap).html(html);
                $(mapWrap).show();
            });
        }
    }
    
    $('input[name="_Price"]').attr('data-slider-min', '<?= floor($minPrice) ?>');
    $('input[name="_Price"]').attr('data-slider-max', '<?= ceil($maxPrice) ?>');
    $('input[name="_Price"]').attr('data-slider-value', '[<?= isset($filters['MinPrice']) ? (int)$filters['MinPrice'] : 0 ?>,<?= isset($filters['MaxPrice']) ? (empty($filters['MaxPrice']) ? 100 : $filters['MaxPrice']) : (empty($maxPrice) ? 100 : ceil($maxPrice)) ?>]');
    $('.data-slider-min').text('<?= floor($minPrice) ?>');
    $('.data-slider-max').text('<?= ceil($maxPrice) ?>');
    
    initPriceSlider(<?= floor($minPrice) ?>, <?= ceil($maxPrice) ?>);
    
</script>