<?php

    use yii\bootstrap\Html;
    use yii\bootstrap\ActiveForm;
    use andrewblake1\creditcard\CreditCardNumber;
    use andrewblake1\creditcard\CreditCardExpiry;
    use andrewblake1\creditcard\CreditCardCVCode;
    
?>

<div class="container">
    <br />
    <?php $form = ActiveForm::begin([
        'options' => [
            'style' => 'width: 400px',
        ]
    ]) ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'HolderName')->textInput() ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'Number')->widget(CreditCardNumber::className()) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'ExpirationDate')->widget(CreditCardExpiry::className()) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'CVV')->widget(CreditCardCVCode::className()) ?>
        </div>
    </div>
    <?= Html::submitButton('Trimite', [
        'class' => 'btn btn-primary',
    ]) ?>
    <?php ActiveForm::end() ?>
</div>