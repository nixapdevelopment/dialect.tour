<?php

namespace app\modules\BackOffice\modules\Booking;

/**
 * booking module definition class
 */
class Booking extends \app\components\Module\BackOfficeModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\BackOffice\modules\Booking\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
