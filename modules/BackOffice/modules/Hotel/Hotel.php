<?php

namespace app\modules\BackOffice\modules\Hotel;

/**
 * hotel module definition class
 */
class Hotel extends \app\components\Module\BackOfficeModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\BackOffice\modules\Hotel\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
