<?php

namespace app\controllers;

use Yii;
use app\controllers\AppController;
use yii\base\Theme;
use yii\web\NotFoundHttpException;


class FrontController extends AppController
{
    
    public function init()
    {
        parent::init();
        
        Yii::$app->view->theme = new Theme([
            'basePath' => '@app/views/themes/front',
            'baseUrl' => '@web/front',
            'pathMap' => [
                '@app/views' => '@app/views/themes/front',
            ]
        ]);
        $this->layout = 'frontend';
    }


    public function actionError()
    {
        throw new NotFoundHttpException("Page not found");
    }
    
}
