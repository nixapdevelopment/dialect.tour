<?php
namespace app\controllers;


use Yii;
use app\controllers\AppController;
use yii\base\Theme;
use yii\web\NotFoundHttpException;
use app\modules\Location\models\Location;
use yii\data\ActiveDataProvider;
use app\modules\Hotel\models\Hotel;
use app\modules\Hotel\models\HotelFrontSearch;
use app\modules\Hotel\models\HotelSearch;
use app\modules\Parser\providers\SunhotelsProvider;

class FrontController extends AppController
{
    public function init()
    {
        parent::init();
        
        Yii::$app->view->theme = new Theme([
            'basePath' => '@app/views/themes/front',
            'baseUrl' => '@web/front',
            'pathMap' => [
                '@app/views' => '@app/views/themes/front',
            ]
        ]);
        $this->layout = 'frontend';
    }


    public function actionError()
    {
        throw new NotFoundHttpException("Page not found");
    }
    
    public function actionCountries()
    {
        $searchModel = new HotelFrontSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => Location::find()->with('lang')->where(['Type' => 'Country']),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('../themes/front/countries',[


            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,

        ]);
    }
    public function actionRegions($id)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Location::find()->with('lang')->where(['Type' => Location::TypeRegion, 'ParentID'=> $id]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('../themes/front/regions',[


            'dataProvider' => $dataProvider,

        ]);
    }
    public function actionCities($id)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Location::find()->with('lang')->where(['Type' => Location::TypeCity, 'ParentID'=> $id]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('../themes/front/cities',[


            'dataProvider' => $dataProvider,

        ]);
    }


    public function actionHotelSearch()
    {
        $searchModel = new HotelFrontSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('../themes/front/hotelSearch', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAutoCompleteLocations($location){
        $query = Hotel::find()->joinWith(['lang','location.lang']);
        $query -> andFilterWhere(['or',
           ['like','HotelLang.Name', $location],
           //['like','LocationLang.Name', $location],
        ]);

        $locations = $query->limit(20)->all();

        $result = [];
        foreach ($locations as $location)
        {
            $result[$location->lang->Name] = [
                'id' => $location->ID,
                'label' => $location->lang->Name .' ( '.$location->country->lang->Name.', '.$location->location->lang->Name.' )',
            ];
        }

        exit(json_encode($result));
    }
    
    public function actionParse()
    {
//        $provider = new SunhotelsProvider();
//        $provider->getLocations();
        
//        $provider = new \app\modules\Parser\providers\HotelbedsProvider();
//        $provider->getLocations();
//        exit;
        
        $provider = new \app\modules\Parser\providers\WhlProvider();
        $provider->getLocations();
        exit;
    }
}
