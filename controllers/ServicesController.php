<?php

namespace app\controllers;


class ServicesController extends FrontController
{
    
    public function actionIndex()
    {
        return $this->render('services');
    }
    
}
