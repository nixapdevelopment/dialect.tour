<?php
namespace app\controllers;

use app\modules\Feedback\models\Feedback;


class AuthController extends FrontController
{



    public function actionIndex()
    {
        $feedback = new Feedback();
        return $this->render('index', [
            'feedback' => $feedback,
        ]);
    }
    
}


