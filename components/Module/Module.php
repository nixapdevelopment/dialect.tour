<?php
namespace app\components\Module;

use Yii;
use yii\base\Module as SystemModule;

class Module extends SystemModule
{
    
    public function init()
    {
        parent::init();
        
        $this->layoutPath = Yii::getAlias('@app/views/themes/buh/layouts');
    }
    
}