<?php
namespace app\components\Enum;

use Yii;

class TipuriArticole extends Enum
{
    
    const Marfuri = 'Marfuri';
    const MateriiPrime = 'MateriiPrime';
    const MaterialeAuxiliare = 'MaterialeAuxiliare';
    const Combustibili = 'Combustibili';
    const PieseDeSchimb = 'PieseDeSchimb';
    const AlteMatConsumabile = 'AlteMatConsumabile';
    const ProduseFinite = 'ProduseFinite';
    const Ambalaje = 'Ambalaje';
    const ObiecteDeInventar = 'ObiecteDeInventar';
    const ProduseReziduale = 'ProduseReziduale';
    const Semifabricate = 'Semifabricate';
    const AmenajariProvizorii = 'AmenajariProvizorii';
    const MatSprePrelucrare = 'MatSprePrelucrare';
    const MatInPastrareConsig = 'MatInPastrareConsig';
    const DiscountFinanciarIntrari = 'DiscountFinanciarIntrari';
    const DiscountFinanciarIesiri = 'DiscountFinanciarIesiri';
    const DiscountComercialIntrari = 'DiscountComercialIntrari';
    const DiscountComercialIesiri = 'DiscountComercialIesiri';
    const ServiciiVandute = 'ServiciiVandute';
    
    public static function items()
    {
        return [
            self::Marfuri => Yii::t('app', 'Marfuri'),
            self::MateriiPrime => Yii::t('app', 'Materii prime'),
            self::MaterialeAuxiliare => Yii::t('app', 'Materiale auxiliare'),
            self::Combustibili => Yii::t('app', 'Combustibili'),
            self::PieseDeSchimb => Yii::t('app', 'Piese de schimb'),
            self::AlteMatConsumabile => Yii::t('app', 'Alte mat. consumabile'),
            self::ProduseFinite => Yii::t('app', 'Produse finite'),
            self::Ambalaje => Yii::t('app', 'Ambalaje'),
            self::ObiecteDeInventar => Yii::t('app', 'Obiecte de inventar'),
            self::ProduseReziduale => Yii::t('app', 'Produse reziduale'),
            self::Semifabricate => Yii::t('app', 'Semifabricate'),
            self::AmenajariProvizorii => Yii::t('app', 'Amenajari provizorii'),
            self::MatSprePrelucrare => Yii::t('app', 'Mat. spre prelucrare'),
            self::MatInPastrareConsig => Yii::t('app', 'Mat. in pastrare/consig.'),
            self::DiscountFinanciarIntrari => Yii::t('app', 'Discount financiar intrari'),
            self::DiscountFinanciarIesiri => Yii::t('app', 'Discount financiar iesiri'),
            self::DiscountComercialIntrari => Yii::t('app', 'Discount comercial intrari'),
            self::DiscountComercialIesiri => Yii::t('app', 'Discount comercial iesiri'),
            self::ServiciiVandute => Yii::t('app', 'Servicii vandute'),
        ];
    }
    
}