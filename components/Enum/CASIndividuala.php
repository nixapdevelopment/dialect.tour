<?php
namespace app\components\Enum;

use Yii;

class CASIndividuala extends Enum
{
    
    const Normala = 10.5;
    const Fara = 0;
    
    public static function items()
    {
        return [
            self::Normala => Yii::t('app', 'Normala 10.5%'),
            self::Fara => Yii::t('app', 'Fara 0%'),
        ];
    }
    
}