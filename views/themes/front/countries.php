<?php

use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\widgets\Pjax;
?>
    <div class="post-index">

        <?php Pjax::begin();?>
        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
        <?php

        echo Html::tag('h2','Search Hotel:');
?>
        <?php Pjax::end(); ?>
    </div>
<?php
Pjax::begin();
echo Html::tag('h2','Countries:');
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_country',
    'pager' => [
        'prevPageLabel' => 'previous',
        'nextPageLabel' => 'next',
        'maxButtonCount' => 5,
    ],

]);

Pjax::end();