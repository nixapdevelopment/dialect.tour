
<?php
    use app\views\themes\front\assets\FrontAsset;
      $bundle = FrontAsset::register($this);
?>


<section class="services">
    <div class="container">
        <div class="title">
            Servicii
        </div>
        <div class="services-group">
            <div class="row">
                <div class="col-md-3">
                    <div class="services-box" data-mh="70">
                        <div class="img-box">
                            <img class="img-responsive" src="<?php echo $bundle->baseUrl ?>/images/rio.jpg" alt="">
                        </div>
                        <div class="title-services-box" data-mh="71">
                            Lorem ipsum lorem
                        </div>
                        <div class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Animi, esse facilis labore molestiae quia soluta.
                            Hic labore nostrum omnis sint voluptatem. Enim error
                            fugit iste labore, nostrum praesentium quasi sit?


                            <div class="text-center">
                                <a class="details" href="#">
                                    Mai multe detalii
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-box" data-mh="70">
                        <div class="img-box">
                            <img class="img-responsive" src="<?php echo $bundle->baseUrl ?>/images/bora.jpg" alt="">
                        </div>
                        <div class="title-services-box" data-mh="71">
                            Lorem ipsum lorem
                        </div>
                        <div class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Animi, esse facilis labore molestiae quia soluta.
                            Hic labore nostrum omnis sint voluptatem. Enim error
                            fugit iste labore, nostrum praesentium quasi sit?

                            <div class="text-center">
                                <a class="details" href="#">
                                    Mai multe detalii
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-box" data-mh="70">
                        <div class="img-box">
                            <img class="img-responsive" src="<?php echo $bundle->baseUrl ?>/images/portugalia.jpg" alt="">
                        </div>
                        <div class="title-services-box" data-mh="71">
                            Lorem ipsum lorem
                        </div>
                        <div class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Animi, esse facilis labore molestiae quia soluta.
                            Hic labore nostrum omnis sint voluptatem. Enim error
                            fugit iste labore, nostrum praesentium quasi sit?


                            <div class="text-center">
                                <a class="details" href="#">
                                    Mai multe detalii
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-box" data-mh="70">
                        <div class="img-box">
                            <img class="img-responsive" src="<?php echo $bundle->baseUrl ?>/images/usa.jpg" alt="">
                        </div>
                        <div class="title-services-box" data-mh="71">
                            Lorem ipsum lorem
                        </div>
                        <div class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Animi, esse facilis labore molestiae quia soluta.
                            Hic labore nostrum omnis sint voluptatem. Enim error
                            fugit iste labore, nostrum praesentium quasi sit?

                            <div class="text-center">
                                <a class="details" href="#">
                                    Mai multe detalii
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt30">
                <div class="col-md-4">
                    <div class="small-services-box">
                        <div class="icon-box">

                        </div>
                        <div class="title">
                            Lorem ipsum
                        </div>
                        <div class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Accusantium animi assumenda distinctio dolorem harum impedit
                            laudantium nulla quis voluptas, voluptate? Aliquid delectus
                            facilis, possimus quaerat saepe similique ullam vel velit.
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="small-services-box">
                        <div class="icon-box">

                        </div>
                        <div class="title">
                            Lorem ipsum
                        </div>
                        <div class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Accusantium animi assumenda distinctio dolorem harum impedit
                            laudantium nulla quis voluptas, voluptate? Aliquid delectus
                            facilis, possimus quaerat saepe similique ullam vel velit.
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="small-services-box">
                        <div class="icon-box">

                        </div>
                        <div class="title">
                            Lorem ipsum
                        </div>
                        <div class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Accusantium animi assumenda distinctio dolorem harum impedit
                            laudantium nulla quis voluptas, voluptate? Aliquid delectus
                            facilis, possimus quaerat saepe similique ullam vel velit.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>